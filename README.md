# Práticas

## Encontro 1

* @practice Classifique um grupo de objetos Learner usando lambda expressions e method references. * @practice Defina métodos estáticos e métodos de instância em um interface

## Encontro 2

* @practice Defina uma classe que implementa a interface @LearnerRepository e se utiliza das interfaces funcionais do pacote java.util.function
 * @practice Defina uma classe que implementa a interface @LearnerRepository e se utilza dos métodos da Stream API

 ## Encontro 3
 
 * @practice Defina exemplos de uso das principais interfaces funcionais do pacote java.util.fuction
 * @practice Defina exemplos de uso dos principais métodos da interface java.util.stream.Stream e java.util.stream.IntStream
 
 ## Encontro 4
 
 * @practice Defina exemplos de uso dos principais tipos beneficiados com novas operações na versão 8: coleções, IO, NIO.2 e classes básicas como Scanner, Pattern/Matcher e Random.
 * @practice Defina exemplos de uso dos principais tipos beneficiados com novas operações nas versões posteriores a versão 8: coleções e String.
 @practice Defina exemplos de uso de outras novidades da versão 8
 
 ## Encontro 5
 
 * @practice Defina exemplos de uso dos principais tipos do pacote java.time
 * @practice Defina exemplos de uso de Javascript usando o motor de scripts Nashorn
 * @practice Defina um exemplo de uso do Fork/Join Framework
 * @practice Defina exemplos de uso para as novidades relacionadas a tratamento de exceções
 * @practice Defina exemplos de uso da nova API de Console IO (NIO.2)
 * @practice Defina exemplos de uso com literais númericas
 * @practice Defina exemplos de uso de outras novidades da versão 7
 