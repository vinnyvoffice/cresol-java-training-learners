package cresol.java.training.learning.java.util;

import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ScannerScenarios {

	@Test
	public void tokens() {
		Scanner sc = new Scanner("a b c 134");
		Stream<String> tokens = sc.tokens();
		Assertions.assertEquals(4, tokens.count());
		//Assertions.assertEquals("a", sc.next());
	}
	
	@Test
	public void findAll() {
		Scanner sc = new Scanner("a b c 134");
		
		Stream<MatchResult> tokens = sc.findAll("\\w");
	}
	
}
