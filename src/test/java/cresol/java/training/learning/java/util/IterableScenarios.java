package cresol.java.training.learning.java.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

public class IterableScenarios {

	@Test
	public void forEach() {
		Collection<String> c = new LinkedList<String>();
		Consumer<? super String> consumer = s -> save(s);
		c.forEach(consumer);
	}
	
	void save(String s) {
		System.out.println("Salvando "+s);
	}
	
}
