package cresol.java.training.learning.java.util.stream;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StreamScenarios {

	@Test
	public void of() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		Assertions.assertEquals(16, s.mapToInt(v -> v.intValue()).sum());
	}

	@Test
	public void map() {
		List<Integer> list = Arrays.asList(1, 3, 5, 7);
		Stream<Integer> s = list.stream();
		Stream<Date> map = s.map(x -> new Date());
		Assertions.assertEquals(4, map.toArray().length);
	}

	@Test
	public void filter() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		Stream<Integer> s2 = s.filter(n -> n > 3);
		Object[] result = s2.toArray();
		Assertions.assertEquals(5, result[0]);
		Assertions.assertEquals(7, result[1]);
	}

	@Test
	public void alreadyClosed() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		Stream<Integer> s2 = s.filter(n -> n > 3);
		s2.toArray();// terminal operation
		Assertions.assertThrows(IllegalStateException.class, () -> s2.count());// terminal operation again
	}

	@Test
	public void peek() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		Predicate<Integer> pred1 = n -> (n > 3);
		Predicate<Integer> pred2 = pred1.and(n -> n % 2 == 1);
		Predicate<Integer> pred = pred2.or(n -> n < 0);
		Stream<Integer> s2 = s.filter(pred).peek(v -> System.out.println(v));
		Object[] result = s2.toArray();
		Assertions.assertEquals(5, result[0]);
		Assertions.assertEquals(7, result[1]);
	}

	@Test
	public void forEach() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		s.filter(n -> n > 3).forEach(v -> System.out.println(v));
		Assertions.assertTrue(true);
	}

	@Test
	public void sum() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		DoubleStream ds = s.mapToDouble(n -> (double) n);
		Assertions.assertEquals(16, ds.sum());
	}

	@Test
	public void average() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		LongStream ds = s.mapToLong(n -> Long.valueOf(n));
		OptionalDouble average = ds.average();
		Assertions.assertEquals(4.0, average.orElse(-1));
	}

	@Test
	public void summaryStatistics() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		LongStream ls = s.mapToLong(n -> Long.valueOf(n));
		LongSummaryStatistics summaryStatistics = ls.summaryStatistics();
		Assertions.assertEquals(1, summaryStatistics.getMin());
		Assertions.assertEquals(7, summaryStatistics.getMax());
		Assertions.assertEquals(4.0, summaryStatistics.getAverage());
		Assertions.assertEquals(16, summaryStatistics.getSum());
	}

	@Test
	public void min() {
		Stream<Integer> s = Stream.of(3, 5, 7, 1);
		Optional<Integer> min = s.min((n1, n2) -> n1 - n2);
		Assertions.assertEquals(1, min.orElse(Integer.MIN_VALUE));
	}

	@Test
	public void max() {
		Stream<Integer> s = Stream.of(3, 5, 7, 1);
		Optional<Integer> min = s.max((n1, n2) -> n1 - n2);
		Assertions.assertEquals(7, min.orElse(Integer.MAX_VALUE));
	}

	@Test
	public void findFirst() {
		Stream<Integer> s = Stream.of(3, 5, 7, 1);
		Optional<Integer> min = s.findFirst();
		Assertions.assertEquals(3, min.orElse(Integer.MAX_VALUE));
	}

	@Test
	public void findAny() {
		Integer[] data = {3, 5, 7, 1};
		Stream<Integer> s = Stream.of(data);
		Optional<Integer> min = s.findAny();
		Assertions.assertTrue(Arrays.asList(data).contains(min.orElse(0)));
	}

	@Test
	public void noneMatch() {
		Integer[] data = {3, 5, 7, 1};
		Stream<Integer> s = Stream.of(data);
		boolean result = s.noneMatch(n -> n < 0);
		Assertions.assertEquals(true, result);
	}

	@Test
	public void anyMatch() {
		Integer[] data = {3, 5, 7, 1};
		Stream<Integer> s = Stream.of(data);
		boolean result = s.anyMatch(n -> n < 0);
		Assertions.assertEquals(false, result);
	}

	@Test
	public void allMatch() {
		Integer[] data = {3, 5, 7, 1};
		Stream<Integer> s = Stream.of(data);
		boolean result = s.anyMatch(n -> n > 0);
		Assertions.assertEquals(true, result);
	}

	@Test
	public void sorted() {
		Stream<Integer> s = Stream.of(3, 5, 7, 1);
		Optional<Integer> result = s.sorted().findFirst();
		Assertions.assertEquals(1, result.orElseThrow());
	}

	@Test
	public void flatMap() {
		List<String> list = Arrays.asList("Flamengo", "Minas", "Bauru");
		Stream<String> s = list.stream();
		Stream<String> flat = s.flatMap(t -> Stream.of(t.split("n")));
		Assertions.assertEquals(5, flat.toArray().length);
	}

	
	@Test
	public void mapOfStreams() {
		List<String> list = Arrays.asList("Flamengo", "Minas", "Bauru");
		Stream<String> s = list.stream();
		Stream<Object> flat = s.map(t -> Stream.of(t.split("n")));
		Assertions.assertEquals(3, flat.toArray().length);
	}

	@Test
	public void reduce() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		Optional<Integer> reduce = s.reduce((v1, v2) -> v1 + v2);
		Assertions.assertEquals(16, reduce.orElseThrow());
	}
	
	@Test
	public void reduceBigDecimals() {
		Stream<BigDecimal> s = Stream.of(BigDecimal.ONE, BigDecimal.ZERO, new BigDecimal("100"));
		Optional<BigDecimal> reduce = s.reduce((v1, v2) -> v1.add(v2));
		Assertions.assertEquals(new BigDecimal("101"), reduce.orElseThrow());
	}


	@Test
	public void collect() {
		Stream<Integer> s = Stream.of(1, 3, 5, 7);
		Set<Integer> set = s.filter(n -> n > 3).collect(Collectors.toSet());
	}

}
