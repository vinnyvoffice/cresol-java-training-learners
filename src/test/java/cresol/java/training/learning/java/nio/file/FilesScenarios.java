package cresol.java.training.learning.java.nio.file;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FilesScenarios {

	@Test
	public void lines() throws IOException {
		Path path = Paths.get("./src/test/java/cresol/java/training/learning/java/io/BufferedReaderScenarios.java");
		Stream<String> lines = Files.lines(path);
		long actual = lines.filter(l -> l.startsWith("import")).count();
		Assertions.assertEquals(6, actual);
	}
	
	/**
	 * @since 1.7
	 */
	@Test
	public void newDirectoryStream() throws IOException {
		Path dir = Paths.get("./src/test/java/cresol/java/training/learning/java/util/");
		DirectoryStream<Path> directoryStream = Files.newDirectoryStream(dir);
		int counter = 0;
		for (Path path:directoryStream) {
			if (path.getFileName().toString().contains(".java")) {
				counter ++;
			}
		}
		Assertions.assertEquals(7, counter);
	}
	
	/**
	 * @since 1.8
	 */
	@Test
	public void find() throws IOException {
		Path start = Paths.get("./src/test/java/cresol/java/training/learning/java/util/");
		BiPredicate<Path, BasicFileAttributes> matcher = (p,a) -> p.getFileName().toString().contains(".java");
		Stream<Path> find = Files.find(start, 1, matcher);
		long counter = find.count();
		Assertions.assertEquals(7, counter);
	}
	
	@Test
	public void walk() {
	}
	
}
