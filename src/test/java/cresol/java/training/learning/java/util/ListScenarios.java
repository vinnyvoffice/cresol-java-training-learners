package cresol.java.training.learning.java.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ListScenarios {

	@Test
	public void removeIf() {
		List<String> names = new LinkedList<>(Arrays.asList("A","B","C","D","E"));
		names.removeIf(n -> n.charAt(0) > 'C');
		Assertions.assertEquals(3, names.size());
	}
	
	@Test
	public void replaceAll() {
		List<String> names = new LinkedList<>(Arrays.asList("A","B","C","D","E"));
		UnaryOperator<String> operator = n -> n.concat(" sobrenome");
		names.replaceAll(operator);
		Assertions.assertEquals("A sobrenome", names.get(0));
	}
	
	@Test
	public void sort() {
		List<String> names = new LinkedList<>(Arrays.asList("B","E","C","D","A"));
		//Collections.sort(names);
		names.sort((n1,n2) -> n1.compareTo(n2));
		names.sort((n1,n2) -> n1.length() - n2.length());
		Function<String,Integer> keyExtractor = n -> n.length();
		names.sort(Comparator.comparing(keyExtractor));
		
	}
	
	@Test
	public void of() {
	}
	
	@Test
	public void copyOf() {
	}
	
}
