package cresol.java.training.learning.java.util;

import java.util.LinkedList;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CollectionScenarios {

	@Test
	public void stream() {
		long counter = IntStream.rangeClosed(1, 10000).filter(n -> n %2 == 0).count();
		Assertions.assertEquals(5000, counter);
	}
	
	@Test
	public void parallelStream() {
		new LinkedList<Integer>().parallelStream();
		long counter = IntStream.rangeClosed(1, 10000).peek(n ->System.out.format("%s %s %n", n, Thread.currentThread())).filter(n -> n %2 == 0).parallel().count();
		Assertions.assertEquals(5000, counter);
	}
	
}
