package cresol.java.training.learning.java.lang;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringScenarios {

	@Test
	public void lines() {
		Stream<String> lines = "a\nb\nc".lines();
		Assertions.assertEquals(3, lines.count());
	}

	@Test
	public void chars() {
		IntStream chars = "a\nb\nc".chars();
		"a\nb\nc".repeat(3);
	}

	@Test
	public void codePoints() {
		IntStream codes = "a\nb\nc".codePoints();

	}

}
