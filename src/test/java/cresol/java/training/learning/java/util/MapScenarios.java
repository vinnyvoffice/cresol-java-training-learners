package cresol.java.training.learning.java.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import org.junit.jupiter.api.Test;

public class MapScenarios {

	@Test
	public void getOrDefault() {}
	@Test
	public void forEach() {
		Map<String,Integer> map = new HashMap<>();
		BiConsumer<? super String, ? super Integer> action = (k,v) -> System.out.println(k+"-"+v) ;
		map.forEach(action);
		
	}
	@Test
	public void replaceAll() {}
	@Test
	public void compute() {
		Map<String,Integer> map = new HashMap<>();
		map.compute("A", (k, v) -> v);
	}
	@Test
	public void computeIfAbsent() {}
	@Test
	public void computeIfPresent() {}
	@Test
	public void merge() {}
	@Test
	public void of() {}
	@Test
	public void ofEntries() {}
	@Test
	public void entry() {}
	@Test
	public void copyOf() {}
	
}
