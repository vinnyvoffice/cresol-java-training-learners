package cresol.java.training.learning.java.lang.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Target;

import org.junit.jupiter.api.Test;

public class RepeatableScenarios {

	@Recomendation("fazer uso de design pattern")
	private String atributo1;
	
	@Tarefas({ @Tarefa("fazer tarefa 1"), @Tarefa("fazer tarefa 2") })
	@Test
	public <@Recomendation("") T> T x(T t) {
		@Recomendation("") String nome = null;
		return t;
	}
}

@Repeatable(Tarefas.class)
@interface Tarefa {
	String value();
}

@interface Tarefas {
	Tarefa[] value();
}

@Target(ElementType.TYPE_USE)
@interface Recomendation {
	String value();
}