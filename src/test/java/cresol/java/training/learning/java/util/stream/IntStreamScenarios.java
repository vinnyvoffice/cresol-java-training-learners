package cresol.java.training.learning.java.util.stream;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class IntStreamScenarios {

	
	@Test
	public void boxed() {
		Stream<Integer> boxed = IntStream.of(1,3,5,7).boxed();
		Stream<Long> boxed2 = LongStream.of(1l,3l,5l,7l).boxed();
		Stream<Double> boxed3 = DoubleStream.of(1,3,5,7).boxed();
	}

	
	@Test
	public void range() {
	}

	@Test
	public void generate() {
	}

	@Test
	public void iterate() {
	}

	@Test
	public void skip() {
	}

	@Test
	public void limit() {
	}

}
