package cresol.java.training.learning.java.util;

import java.util.Random;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RandomScenarios {

	@Test
	public void ints() {
		IntStream ints2 = new Random().ints(1000, 1, 100);
		Assertions.assertTrue(ints2.sum() >= 1000);
	}
	
	@Test
	public void longs() {}
	
	@Test
	public void doubles() {}
	
}
