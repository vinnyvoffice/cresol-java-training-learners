package cresol.java.training.learning.java.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BufferedReaderScenarios {

	@SuppressWarnings("resource")
	@Test
	public void lines() throws FileNotFoundException {
		Stream<String> lines = new BufferedReader(
				new FileReader("./src/test/java/cresol/java/training/learning/java/io/BufferedReaderScenarios.java")
				).lines();
		Assertions.assertEquals(22, lines.count());
	}
	
}
