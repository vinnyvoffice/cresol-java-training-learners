package cresol.java.training.learners.lesson2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LearnerRepositoryJava7Test {

	private final Learner[] learners = new Learner[] {
		new Learner("vinny", true, "Smalltalk", 2000),
		new Learner("mauricio", false, "Java", 2010)
	};
	@Test
	public void selectAllWhere() {
		LearnerRepository rep = new LearnerRepositoryJava7(learners);
		Learner[] result = rep.selectAllWhere(l -> l.name.startsWith("m"));
		Assertions.assertEquals("mauricio", result[0].name);
	}
}
