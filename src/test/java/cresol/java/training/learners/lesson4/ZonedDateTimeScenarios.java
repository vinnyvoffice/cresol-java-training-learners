package cresol.java.training.learners.lesson4;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ZonedDateTimeScenarios {

	
	@Test
	public void of() {
		LocalDate date = LocalDate.of(2019, 10, 23);
		LocalDateTime localDateTime = date.atTime(LocalTime.of(18, 30));
		ZoneId zone = ZoneId.of("America/Cuiaba");
		ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, zone);
		Assertions.assertEquals("2019-10-23T18:30-04:00[America/Cuiaba]", zonedDateTime.toString());
	}
	
	@Test
	public void sameInstant() {
		LocalDate date = LocalDate.of(2019, 10, 23);
		LocalDateTime localDateTime = date.atTime(LocalTime.of(18, 30));
		ZoneId cuiaba = ZoneId.of("America/Cuiaba");
		ZoneId saoPaulo = ZoneId.of("America/Sao_Paulo");
		ZonedDateTime dataHorarioCuiaba = ZonedDateTime.of(localDateTime, cuiaba);
		ZonedDateTime dataHorarioSaoPaulo = dataHorarioCuiaba.withZoneSameInstant(saoPaulo);
		Assertions.assertEquals("2019-10-23T19:30-03:00[America/Sao_Paulo]", dataHorarioSaoPaulo.toString());
	}
}
