package cresol.java.training.learners.lesson4;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjuster;
import java.util.Locale;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LocalDateScenarios {

	private static final DateTimeFormatter MY_DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd:MMMM:yy", new Locale("pt", "BR"));

	@Test
	public void of() {
		LocalDate date = LocalDate.of(2019, 10, 23);
		String format = date.format(MY_DATETIME_FORMATTER);
		Assertions.assertEquals("23:outubro:19", format);
	}
	
	@Test
	public void parse() {
		LocalDate actual = LocalDate.parse("23:outubro:19",MY_DATETIME_FORMATTER);
		LocalDate expected = LocalDate.of(2019, 10, 23);
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	public void with() {
		LocalDate actual = LocalDate.parse("23:outubro:19",MY_DATETIME_FORMATTER);
		TemporalAdjuster adjuster = ld -> ld.plus(Period.of(1, 2, 0));
		actual = actual.withDayOfMonth(25).withYear(2020).with(adjuster);
		LocalDate expected = LocalDate.of(2021,12,25);
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	public void at() {
		LocalDate date = LocalDate.of(2019, 10, 23);
		LocalDateTime atTime = date.atTime(LocalTime.of(18, 30));
		LocalDateTime actual = atTime.plusHours(2).plusMonths(3).minusMinutes(10);
		Assertions.assertEquals(LocalDateTime.parse("2020-01-23T20:20"), actual);
	}
}
