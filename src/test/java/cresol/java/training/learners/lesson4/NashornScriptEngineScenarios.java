package cresol.java.training.learners.lesson4;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NashornScriptEngineScenarios {

	@Test
	public void eval() throws ScriptException {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("nashorn");
		Object eval = engine.eval("function upper(value) { return value.toUpperCase();}");
		Assertions.assertEquals(null, eval);
		Object actual = engine.eval("function upper(value) { return value.toUpperCase();} ; upper('cresol'); ");
		Assertions.assertEquals("CRESOL", actual.toString());
	}
}
