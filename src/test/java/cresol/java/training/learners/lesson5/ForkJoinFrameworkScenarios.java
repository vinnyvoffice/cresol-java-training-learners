package cresol.java.training.learners.lesson5;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ForkJoinFrameworkScenarios {

	@Test
	public void xx() {
		ForkJoinPool pool = ForkJoinPool.commonPool();
		ForkJoinTask<Integer> maxTask = new MainTask(1000);
		int max = pool.invoke(maxTask);
		Assertions.assertEquals(7, max);
	}
}

class MainTask extends RecursiveTask<Integer> {

	private int value;
	
	MainTask(int value){
		this.value = value;
	}
	@Override
	protected Integer compute() {
		Integer result = value;
		if (deveQuebrar()) {
			MainTask left = new MainTask(value/2);
			MainTask right = new MainTask(value/2);
			left.fork();
			result = Math.max(left.join(), right.compute());
		}
		return result;
	}

	private boolean deveQuebrar() {
		return value > 10;
	}
	
}
