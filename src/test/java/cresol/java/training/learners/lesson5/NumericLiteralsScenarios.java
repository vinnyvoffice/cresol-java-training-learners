package cresol.java.training.learners.lesson5;

public class NumericLiteralsScenarios {

	public void octal() {
		Integer value = 0724534545;
	}
	
	public void hexa() {
		Integer value = 0xFF1AB;
	}
	
	public void binary() {
		Integer value = 0b0100_10100;
	}
	
	public void decimal() {
		Integer value = 1_000_000;
		Double value2 = 33_1.010;
	}
}
