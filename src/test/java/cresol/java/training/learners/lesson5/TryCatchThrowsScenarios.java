package cresol.java.training.learners.lesson5;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

public class TryCatchThrowsScenarios {

	@Test
	public void legacy()  {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(null);
			conn.createStatement().execute("update xx");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void try_with_resources() {
		try(AutoCloseable conn = () -> System.out.println("fechando recurso"); Connection xxxx = null){
			System.out.println("executando alguma coisa");
		} catch (Exception e) {
			System.out.println("deu erro na execucao");
			e.printStackTrace();
		}
		System.out.println("segue o jogo!");
	}
	
	@Test
	public void precise_rethrowing() throws SQLException, IOException {
		try{
			System.out.println("executando alguma coisa");
			new File("").createNewFile();
			DriverManager.getConnection(null);
		//} catch (SQLException | IOException  e) {
		} catch (Exception  e) {
			System.out.println("deu erro na execucao");
			throw e;
		}
		System.out.println("segue o jogo!");
	}
}
