package cresol.java.training.learners.lesson2;

public class Learner {

	public final String name;
	public final boolean java8;
	public final String language;
	public final int hiredYear;
  
	public static final Learner[] LEARNERS = new Learner[] {
			new Learner("vinny", true, "Smalltalk", 2000),
			new Learner("vinicius", false, "VisualG", 2015),
			new Learner("djessica", false, "Java", 2016),
			new Learner("thales", false, "Java", 2012),
			new Learner("andre", false, "Pascal", 2015),
			new Learner("thomaz", false, "Java", 2016),
			new Learner("mauro", false, "Java", 2017),
			new Learner("roberto", false, "Java", 2017),
			new Learner("israel", false, "C", 2014),
			new Learner("mauricio", false, "Java", 2011)
	};
	
	public Learner(String name, boolean java8, String language, int hiredYear) {
		super();
		this.name = name;
		this.java8 = java8;
		this.language = language;
		this.hiredYear = hiredYear;
	}
	
}

