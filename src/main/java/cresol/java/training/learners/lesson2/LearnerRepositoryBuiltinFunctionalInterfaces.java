package cresol.java.training.learners.lesson2;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.IntFunction;
import java.util.function.Predicate;

public class LearnerRepositoryBuiltinFunctionalInterfaces implements LearnerRepository {

	private final Learner[] data;
	
	public LearnerRepositoryBuiltinFunctionalInterfaces(Learner[] data) {
		super();
		this.data = data;
	}

	@Override
	public Learner[] selectAll() {
		return data;
	}

	@Override
	public Learner[] selectAllWhereLanguageIs(String language) {
		 return selectAllWhere(learner -> language.equals(learner.language));
	}
	
	public Learner[] selectAllWhere(Predicate<Learner> predicate) {
		 List<Learner> result = new LinkedList<Learner>();
		 for (Learner learner: data) {
			 if (predicate.test(learner)) {
				 result.add(learner);
			 }
		 }
		return result.toArray(Learner[]::new);
	}
	
	public Learner[] xxx(String language) {
		 List<Learner> result = new LinkedList<Learner>();
		 for (Learner learner: data) {
			 if (language.equals(learner.language)) {
				 result.add(learner);
			 }
		 }
		return result.toArray(new Learner[0]);
	}

	@Override
	public Learner[] selectAllSortedBy(Comparator<Learner> comparator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double calculateHiredYearAverage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double calculateHiredYearAverage(Predicate<Learner> predicate) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double calculateHiredYearAverage(Predicate<Learner> predicate, Comparator<Learner> comparator) {
		// TODO Auto-generated method stub
		return 0;
	}

}
