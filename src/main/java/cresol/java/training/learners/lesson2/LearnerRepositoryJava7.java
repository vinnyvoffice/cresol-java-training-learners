package cresol.java.training.learners.lesson2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class LearnerRepositoryJava7 implements LearnerRepository {

	private final Learner[] data;

	public LearnerRepositoryJava7(Learner[] data) {
		super();
		this.data = data;
	}

	@Override
	public Learner[] selectAll() {
		return data;
	}

	@Override
	public Learner[] selectAllWhereLanguageIs(String language) {
		return selectAllWhere(new Predicate<Learner>() {
			@Override
			public boolean test(Learner l) {
				return language.equals(l.language);
			}
		});
	}

	@Override
	public Learner[] selectAllWhere(Predicate<Learner> predicate) {
		List<Learner> result = new LinkedList<>();
		for(Learner learner: data) {
			if (predicate.test(learner)) {
				result.add(learner);
			}
		}
		return result.toArray(new Learner[0]);
	}

	@Override
	public Learner[] selectAllSortedBy(Comparator<Learner> comparator) {
		Arrays.sort(data, comparator);
		return data;
	}

	@Override
	public double calculateHiredYearAverage() {
		double result = 0.0;
		for(Learner learner: data) {
			result += learner.hiredYear;
		}
		return result / data.length;
	}

	@Override
	public double calculateHiredYearAverage(Predicate<Learner> predicate) {
		double result = 0.0;
		int counter = 0;
		for(Learner learner: data) {
			if (predicate.test(learner)) {
				result += learner.hiredYear;
				counter ++;
			}
		}
		return result / counter;
	}

	@Override
	public double calculateHiredYearAverage(Predicate<Learner> predicate, Comparator<Learner> comparator) {
		double result = 0.0;
		int counter = 0;
		Arrays.sort(data, comparator);
		for(Learner learner: data) {
			if (predicate.test(learner)) {
				result += learner.hiredYear;
				counter ++;
			}
		}
		return result / counter;
	}

}
