package cresol.java.training.learners.lesson2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class LearnerRepositoryStreamAPI implements LearnerRepository {

	private final Learner[] data;

	public LearnerRepositoryStreamAPI(Learner[] data) {
		super();
		this.data = data;
	}

	@Override
	public Learner[] selectAll() {
		return data;
	}

	@Override
	public Learner[] selectAllWhereLanguageIs(String language) {
		// TODO Auto-generated method stub
		return null;
	}

	public Learner[] selectAllWhere(Predicate<Learner> predicate) {
		 List<Learner> result = new LinkedList<Learner>();
		 result.addAll(Arrays.asList(data));
		 return result.stream().filter(predicate).map(l -> l).toArray(Learner[]::new);
	}
	
	public String[] selectNamesWhere(Predicate<Learner> predicate) {
		 List<Learner> result = new LinkedList<Learner>();
		 result.addAll(Arrays.asList(data));
		 return result.stream().filter(predicate).map(l -> l.name).toArray(String[]::new);
	}
	
	public Integer[] selectYearsWhere(Predicate<Learner> predicate) {
		 List<Learner> result = new LinkedList<Learner>();
		 result.addAll(Arrays.asList(data));
		 return result.stream().filter(predicate).map(LearnerRepositoryStreamAPI::getYear).toArray(Integer[]::new);
	}

	public static Integer getYear(Learner learner) {
		return learner.hiredYear;
	}
	
	@Override
	public Learner[] selectAllSortedBy(Comparator<Learner> comparator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double calculateHiredYearAverage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double calculateHiredYearAverage(Predicate<Learner> predicate) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double calculateHiredYearAverage(Predicate<Learner> predicate, Comparator<Learner> comparator) {
		// TODO Auto-generated method stub
		return 0;
	}

}
