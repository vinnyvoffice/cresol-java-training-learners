package cresol.java.training.learners.lesson2;

import java.util.Comparator;
import java.util.function.Predicate;

/**
 * @practice Implemente uma classe que implementa essa interface e se utiliza das interfaces funcionais do pacote java.util.function
 * @practice Implemente uma classe que implemente essa interface e se utilza dos métodos da Stream API
 * 
 * @author vinny
 *
 */
public interface LearnerRepository {

	Learner[] selectAll();
	Learner[] selectAllWhereLanguageIs(String language);
	Learner[] selectAllWhere(Predicate<Learner> predicate);
	Learner[] selectAllSortedBy(Comparator<Learner> comparator);
	double calculateHiredYearAverage();
	double calculateHiredYearAverage(Predicate<Learner> predicate);	
	double calculateHiredYearAverage(Predicate<Learner> predicate,Comparator<Learner> comparator);	
	
}
